#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define MAX_PATH_SIZE 1000
#define TOTAL_CHR_SINBLING  2
#define SPACE_PER_DEPTH  3
#define CHR_SINBLING  "\342\224\200"
#define VERTICAL_LINE "\342\224\202"
#define CHR_SINBLING  "-"
#define VERTICAL_LINE "|"

void lsr(char *, int);
void print_name(char *, char *, int);


static const char* COLORS []  =  {
    "\033[0;31m",              // RED
    "\033[0;41m",              // HLRED
    "\033[1;31m",              // BRED
    "\033[0;32m",              // GREEN
    "\033[0;42m",              // HLGREEN
    "\033[1;32m",              // BGREEN
    "\033[0;33m",              // YELLOW
    "\033[0;43m",              // HLYELLOW
    "\033[1;33m",              // BYELLOW
    "\033[0;34m",              // DARKBLUE
    "\033[0;44m",              // HDARKBLUE
    "\033[1;34m",              // BDARKBLUE
    "\033[0;35m",              // PURPLE
    "\033[0;45m",              // HPURPLE
    "\033[1;35m",              // BPURLE
    "\033[0;36m",              // BLUE
    "\033[0;46m",              // HBLUE
    "\033[1;36m",              // BBLUE
    "\033[0;37m",              // WHITE
    "\033[0;47m",              // HWHITE
    "\033[1;37m",              // BBLUE
    "\033[0m"                  // NOCOLOR
};
enum _COLOR_INDEX {
    RED = 0,
    HLRED = 1,
    BRED = 2,
    GREEN = 3,
    HGREEN = 4,
    BGREEN = 5,
    YELLOW = 6,
    HYELLOW = 7,
    BYELLOW = 8,
    DARKBLUE = 9,
    HDARKBLUE = 10,
    BDARKBLUE = 11,
    PURPLE = 12,
    HPURPLE = 13,
    BPURPLE = 14,
    BLUE = 15,
    HBLUE = 16,
    BBLUE = 17,
    WHITE = 18,
    HWHITE = 19,
    BWHITE = 20,
    NOCOLOR = 21,
};


int
main(int argc, char* argv[]) {
    int i = 1;
    if(argc > 1) {
        for(;i < argc; i++) {
            lsr(argv[i], 0);
        }
    }
    else
        lsr(".", 0);
}



void
lsr(char* path, int depth) {
    DIR            *dir;
    struct dirent  *ent;
    struct stat     st;

    if((dir=opendir(path)) != NULL) {
        while((ent=readdir(dir)) != NULL) {
            int is_cur = strcmp(ent->d_name, ".");
            int is_par = strcmp(ent->d_name, "..");
            if(is_cur == 0 || is_par == 0) continue;

            char *abs_ent[MAX_PATH_SIZE];
            snprintf(abs_ent, MAX_PATH_SIZE, "%s/%s", path, ent->d_name);

            stat(abs_ent, &st);
            int is_dir = S_ISDIR(st.st_mode);

            fprintf(stdout, "%s", VERTICAL_LINE);

            if(depth > 0) {
                int i = 0;
                for(;i < depth*SPACE_PER_DEPTH;i++) {
                    if(i > 0 && i % SPACE_PER_DEPTH == 0)
                        fprintf(stdout, VERTICAL_LINE);
                    else
                        fprintf(stdout, " ");
                }
                fprintf(stdout, VERTICAL_LINE);
            }

            int i = 0;
            for(;i < TOTAL_CHR_SINBLING;i++)
                fprintf(stdout, CHR_SINBLING);

            print_name(abs_ent, ent->d_name, is_dir);

            if(is_dir) {
                lsr(abs_ent, depth+1);
            }
        }
    } else {
        fprintf(stderr, "Failed to open %s\n", path);
    }
}


void
print_name(char *abs, char *ent_name, int is_dir) {
    if(is_dir)
        fprintf(stdout, COLORS[BLUE]);

    fprintf(stdout, "%s", ent_name);
    fprintf(stdout, COLORS[NOCOLOR]);
    fprintf(stdout, "\n");
}
