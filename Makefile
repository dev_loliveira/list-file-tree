CC = gcc
CFLAGS = -Wall -g
LDFLAGS =
SOURCES = tree.c
OBJECTS = $(SOURCES:.cpp=.o)
EXECUTABLE = tree

.PHONY: all

all : $(EXECUTABLE)

$(EXECUTABLE) : $(OBJECTS)
	${CC} ${CFLAGS} ${LDFLAGS} $(OBJECTS) -o $(EXECUTABLE)

.cpp.o :
	${CC} ${CFLAGS} ${LDFLAGS} -c $< -o $@

clean :
	find -name "*.o" -delete
	rm $(EXECUTABLE)
