import os
import sys

CHR_SINBLING = "\342\224\200"
TOTAL_CHR_SINBLING = 2
SPACE_PER_DEPTH = 3
VERTICAL_LINE = "\342\224\202"

RED="\033[0;31m"
HLRED="\033[0;41m"
BRED="\033[1;31m"
GREEN="\033[0;32m"
HLGREEN="\033[0;42m"
BGREEN="\033[1;32m"
YELLOW="\033[0;33m"
HLYELLOW="\033[0;43m"
BYELLOW="\033[1;33m"
DARKBLUE="\033[0;34m"
HLDARKBLUE="\033[0;44m"
BDARKBLUE="\033[1;34m"
PURPLE="\033[0;35m"
HLPURPLE="\033[0;45m"
BPURPLE="\033[1;35m"
BLUE="\033[0;36m"
HLBLUE="\033[0;46m"
BBLUE="\033[1;36m"
WHITE="\033[0;37m"
HLWHITE="\033[0;47m"
BWHITE="\033[1;37m"
NOCOLOR="\033[0m"



def lsr(path=".", depth=0):
    global CHR_SINBLING
    global TOTAL_CHR_SINBLING
    global SPACE_PER_DEPTH

    out_handle = sys.stdout

    if depth == 0:
        out_handle.write(path+"\n")

    el = os.listdir(path)
    f_list = [e for e in el if not os.path.isdir(os.path.join(path, e))]
    d_list = [e for e in el if os.path.isdir(os.path.join(path, e))]

    for e in el:
        abs_e = os.path.join(path, e)
        is_d = os.path.isdir(abs_e)

        out_handle.write(VERTICAL_LINE)

        if depth > 0:
            for i in range((depth*SPACE_PER_DEPTH)):
                if i % SPACE_PER_DEPTH == 0 and i > 0:
                    out_handle.write(VERTICAL_LINE)
                else:
                    out_handle.write(" ")
            out_handle.write(VERTICAL_LINE)

        for i in range(TOTAL_CHR_SINBLING):
            out_handle.write(CHR_SINBLING)

        out_handle.write(" ")
        if is_d:
            out_handle.write(BLUE)
        out_handle.write(e)
        out_handle.write(NOCOLOR)
        out_handle.write("\n")

        if os.path.isdir(abs_e):
            lsr(path=abs_e, depth=depth+1)


lsr()
